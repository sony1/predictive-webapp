
var persist = require("persist");
var type = persist.type;

module.exports = persist.define("Taxi", {
  "name": { type: type.STRING }
});
