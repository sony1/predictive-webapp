
var persist = require("persist");
var type = persist.type;

module.exports = persist.define("Booking", {
  "name": { type: type.STRING }
});
