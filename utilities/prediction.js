/*
 * for prediction engine from mysql db
 */

var persist = require("persist");
var queue = require("./queue")
var models = require("../models");

exports.prediction = function(req, res, next) {
	persist.connect(function(err, connection) {
		if (err) {
			next(err);
			return;
		}

		sql = "select distinct pickup_area, count(*) as `total_pickup` from histories group by pickup_area order by `total_pickup` limit 1";

		connection.runSqlAll(sql, [], function(err, results) {
			strJson = results[0];
			queue.publish(strJson);
			res.redirect("home");
		});
	});
};
